# Cover-Rest Example for Java 

To show the usage of Cover-Rest for a Java project, we have chosen the ODFDOM library of the [ODF Toolkit](http://github.com/tdf/odftoolkit).

## The new Idea behind Cover-Rest

The new idea behind Cover-Rest has been explained [here](./coverrest-usage.md).

## Creating Test Coverage with Java

As mentioned in our [Basics of Code Coverage](./code-coverage.md) at this time the most advanced open-source Coverage library is [JaCoCo](https://github.com/jacoco/jacoco/) which unfortunately creates its own binary coverage format.

### Creating a Cobertura Coverage file for a single Java Test

For a quick test the coverage can be created with a script or by command-line.

We used a bash script [create-single-test-coverage.sh](https://gitlab.com/cover-rest/odftoolkit/-/blob/coverage/odfdom/create-single-test-coverage.sh). You may try it with [our odftoolkit fork on the coverage branch](https://gitlab.com/cover-rest/odftoolkit).

1. Clone and build the ODFDOM project of the odftoolkit in odftoolkit/odfdom to have the JAR in the ./target output directory.
**NOTE:** The parameter -DskipTests=true -D"maven.javadoc.skip=true" will neglect tests and JavaDoc creation to save time) 
```bash
    git clone https://gitlab.com/cover-rest/odftoolkit odftoolkit-coverage
    cd odftoolkit-coverage
    mvn install -DskipTests=true -D"maven.javadoc.skip=true"
```

2. In the beginning we defined four constants that we repeatedly used in our script ***(your values might vary)***. If run line by line by bash you would need to replace the variables
```bash
    # ODT feature document trunc name, e.g src/test/resources/test-input/feature/
    ODF_FILE_NAME__TRUNC="table_merged-cells"    
    The name of the feature is also the prefix of the name of the test document.

    # Version number of the Jacoco release being used for testing, required to identify its JAR
    JACOCO_AGENT_VERION="0.8.7"        
    
    # Maven .m2 repository parent directory, required to access the JaCoCo JAR
    MAVEN_REPO_PATH=$HOME/.m2
    
    # Version nmber of the ODFDOM being used
    ODFDOM_VERSION=0.10.0-SNAPSHOT
```

3. The first burden was to create a single coverage file for a Java Test. We used Maven build system using its SureFire Test plugin, which can be called by command line via
We were in need of calling the Jacoco agent directly using its jar from the user's ~/.m2 directory. 
To run a single test and create its JaCoco binary file at target/jacoco.exec file the following have to be called:
```bash
mvn surefire:test -Dtest=FeatureLoadTest -DtextFeatureName=${ODF_FILE_NAME__TRUNC} \
    -DargLine=-javaagent:$MAVEN_REPO_PATH/repository/org/jacoco/org.jacoco.agent/${JACOCO_AGENT_VERION}/\
    org.jacoco.agent-${JACOCO_AGENT_VERION}-runtime.jar=destfile=./target/jacoco.exec
```

4. The JaCoCo library allows to transform the binary target/jacoco.exec into a HTML coverage report and Jacoco XML Coverage file (./target/site/jacoco/jacoco.xml) via 
```bash
mvn jacoco:report
```

5. Initially we wanted to keep the coverage file in our source base, later we added the coverage data into a data base to align them with the commits (the state of the software).
Therefore, copied the file from the volatile ./target directory into the source tree 'src/test/resources/test-input/feature/coverage/'
We are using for the JaCoCo Coverage XML file **the prefix "jacoco_"**.
```bash
cp ./target/site/jacoco/jacoco.xml src/test/resources/test-input/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}.cov.xml
```

6. Optional but helpful for comparance and ease of viewing the Coverage files, we always normalized them by XML pretty-printing/indentation using xmllint.  
xmllint comes with libxml2-utils: https://gitlab.gnome.org/GNOME/libxml2
We are appending for the normalized file **to the trunc the suffix "_indent"**.
```bash
xmllint --format src/test/resources/test-input/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}.cov.xml > \
    src/test/resources/test-reference/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}_indent.cov.xml
```

7. The Cobertura XML file will be simplified!
By default the via JaCoCo generated Cobertura Coverage XML files contains also an entry for every line, even those not being covered. As we have the full lines by the source code this redundant information is being removed.
We are using for the Cobertura Coverage XML file **the prefix "cobertura_"**.
Via the Cover-Diff module using the path of a single Cobertura XML file:
```bash
java -cp ./target/odfdom-java-${ODFDOM_VERSION}-jar-with-dependencies.jar \
    org.odftoolkit.odfdom.changes.CoberturaXMLHandler \
    ./cobertura_${ODF_FILE_NAME__TRUNC}.cov.xml
```

8. We copy this in our code repository as future reference using **the suffix "_stripped"**.
```bash
cp target/test-classes/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml \
    src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml
```

9. We are normalizing the file and appending for the normalized file **to the trunc the suffix "_indent"**.
```bash
xmllint --format src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml \
    > src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped_indent.cov.xml
```
