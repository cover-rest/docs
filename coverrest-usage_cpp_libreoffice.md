# Cover-Rest Example for C++

To show the usage of Cover-Rest for a C++ project, we have chosen the [LibreOffice office suite](https://www.libreoffice.org/) for [ODF](https://github.com/oasis-tcs/odf-tc).

## The new Idea behind Cover-Rest

The new idea behind Cover-Rest has been explained [here](./coverrest-usage.md).

## Creating Test Coverage with C++

As mentioned in our [Basics of Code Coverage](./code-coverage.md) at this time the most advanced open-source Coverage library is [GCOV](https://en.wikipedia.org/wiki/Gcov).

GCOV allows the storage of Coverage data as Cobertura XML file.

A graphical front-end for GCC's coverage testing tool gcov is[LCOV](https://wiki.documentfoundation.org/Development/Lcov). [LCOV is also used by LibreOffice](https://wiki.documentfoundation.org/Development/Lcov).



### Creating a Cobertura Coverage file for a single C++ Test

...