# Cover-Rest Usage

## The new Idea behind Cover-Rest

[Coverage of tests](./code-coverage.md) is usually only a metric to show how much of a source code is being executed by a test.

We take two coverages and subtract them to identify the difference.

Ideally, both test only differ from a single feature, like, for instance, loading in LibreOffice once an ODT document with an image and in the other test loading the very similar ODT docuemnt but without the image. In this case, the difference between the two coverages would identify the source code lines, which exclusively deal with the image feature.

By loading (or saving) in the test documents with only a feature difference the differential feature can be located in the source code. At least the code that is only used by this feature, there is of course still shared code.

Cover-Rest is creating such a diff from two Cobertura XML files, which are compared by our module [Cover-Diff](./https://gitlab.com/cover-rest/cover-diff)

From several such feature tests a map of features could be created we named [Code Cognita](https://gitlab.com/cover-rest/docs/-/blob/main/docs/1_code-cognita.md).

## The main challenge for Enduser

The main challenge of the end user is to create two tests with a meaningful feature difference for his domain and softeware. 

In our case it is easy as the file format ODF provides an interoperabilty of ODF features (e.g. paragraph, image, table, etc.) that can be placed in an ODF document.

But also having two tests, where the difference is the usage of a different GUI component can identify the exclusive source code of this GUI component in a large code base.