# Code Coverage Modularization

## Why doing a Code Coverage Modularization?

We realized that current coverage software was not modularized. Every coverage program always contained every module:

* the creation of coverag data for a certain language
* a file format to seralize this coverage data
* some user's front-end to visualize this coverage data

There was no way to reuse the visualization of coverage data and to store the coverage data into a data base for later comparison and exchanging only the language specific part.

Therefore we wanted to show an example how the multiple coverage software - each specific to some language - can be grouped by a REST API as abstraction layer to resuse a majority of software (e.g. result database and coverage viewer as front-end). 

In this project [we used the Cobertura XML Coverage report serialization as common denominator between our projects and tools](https://gitlab.com/cover-rest/docs/-/blob/main/docs/code-coverage.md#interoperabiltiy-among-coverage-tools).

## Languages of Code Coverage

We used three languages with their atm most common open-source coverage frameworks:

1. For Java - [JaCoCo](https://www.jacoco.org/jacoco/trunk/doc/)
2. For C++ - [Gcov](https://en.wikipedia.org/wiki/Gcov) and [llvm-cov](https://llvm.org/docs/CommandGuide/llvm-cov.html)
3. For Python - [Coverage.py](https://coverage.readthedocs.io/)

## Interoperability by Coverage File Format

As common denominator of as serialization format of the Test Coverage the Cobertura XML format has been choosen. Although the Cobertura project is no longer maintained the C++ & Python libraries allowed the export to this format and the JaCoCo XML format could be [transformed](https://github.com/rix0rrr/cover2cover/) to Cobertura XML.

### New Reusable Coverage Modules

We started to create and reuse the following opensource modules for code coverage

### Coverage API

Our Coverage API should be the glue code between the following three components:

1. Language specific software creating the coverage reports
2. Database keeping all relevant information of past coverage tests (input, coverage data related to source code state (commit hash))
3. Front-End viewing the data

### Coverage Visualization

As common denominator for the visualization we used a [Coverage front-end from Jenkins](https://plugins.jenkins.io/code-coverage-api/) maintained by [Ullrich Hafner](https://github.com/uhafner).

### Coverage Database

A database that allows to save the semantics usually in a coverage (XML) file and allows in addition queries on the coverage data!