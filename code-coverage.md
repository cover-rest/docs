# What is Code Coverage?

## Code Coverage in a Nutshell

[Code Coverage](https://en.wikipedia.org/wiki/Code_coverage) (or test coverage) states how much the source code of a program is being executed when a test (or test suite) is being executed.

Note: A 100% Code Coverage does not proof that the code is working correctly (as intended), as a test might be wrong or some expected functionality might be missing in test and source code. Still it raises the likelihood of correct code as it . :-)

## How does Code Coverage work?

There are fundamentally two ways:

1) The code is being statically analysed. Meaning, the ways data "flows" through the program is anlayzed. For instance, Code that is never reached (tested) can be spotted based on the possible execution input data.
2) The code is being 'instrumentalized'. Either the source code during complization or already the binary code using a tool (like for Java [ASM](https://asm.ow2.io/)).
By this the code being passed is being logged out (like throwing bread crums or making line notes during manual debug steps).

All coverage software we are aware of is following the easier second approach. For instance, [JaCoCo](https://github.com/jacoco/jacoco/) for Java is using under the hood the mentioned Java ByteCode instrumentalization framework [ASM](https://asm.ow2.io/). Aside of the simplicity of 'just' running the tests, it has the advantage that this approach works even when the Byte Code is available.

## Interoperabiltiy among Coverage Tools

There is little interoperabilty among Coverate tools. There are many coverage tools, sometimes multiple for one language. These tools provide aside of the necessary programming language specific tooling as well an own coverage serialization (file format) and front-end. That is the reason for our attempt of [modularization](use-case-3__code-coverage-modularization.md).

Surprisingly, the old Cobertura XML serialization of a coverage report - [Cobertura](https://github.com/cobertura/) is a Java coverage tool no longer maintained - is not only supported by the C++ coverage tool [GCOV](https://en.wikipedia.org/wiki/Gcov), but also the [Cover2Cover](https://github.com/rix0rrr/cover2cover/) transformation ([we improved](https://gitlab.com/cover-rest/cover2cover)) exist to map from the maintained [JaCoCo](https://github.com/jacoco/jacoco) proprietary format to the Cobertura XML.

Therefore, we used this Cobertura XML Format as base for our interoperabilty during this project.