# Code Cognita - Finding Features in Source Code

We were extending the way of [Code Coverage](#what-is-code-coverage) to be able to locate features in existing software in a map, we call Code Cognita (derived from the ancient name [Terra Incognita](https://en.wikipedia.org/wiki/Terra_incognita)).

## The Purposes of a Code Cognita

1. One purpose of the Code Cognita was that in large code bases - as LibreOffice - it is hard for newcomers to identify the location of existing features in the source coce (which source file, which line).
2. The other purpose was the creation of [Fast Feature Tests](2_fast-feature-test.md)!

## The Creation of a Code Cognita

If there are two tests, which input differ only by a single feature (atomic functionaltiy known by the user). For instance:

1. Loading with LibreOffice an office document containing only a paragraph with the text "Hello World".
2. Loading with LibreOffice an office document containing a paragraph with the text "Hello World", but **this time we inserted as well an image**!

We know that the second coverage test (document) would contain only the image feature in addition to the first smaller test.
By substracting the first coverage lines from the larger second coverage lines, we will receive the lines that are only being used for the image feature.
