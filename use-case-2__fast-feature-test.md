# Fast Feature Tests

## Creating Fast Feature Tests from Code Cognita

By a [Code Cognita](#what-is-code-cognita) we are able to identify the position of a feature within the source code.
Our plan was to trigger feature tests, whenever the area of a certain feature had been changed by a commit from a developer.
In other words: After the detection of features within the sources the lines being recently changed should reflect the feature to be tested.

During our work we realized that there exist scenarios where a non feature specific (common) area of the source code has been altered influencing the feature area. Think of data flowing like water throught the programm and upstream the water would be redirected never reaching some feature areas downstream - this might happen when you alter Macros in C++ (black magic).

## Reason for Fast Feature Tests

Giving immediate response to developers committing a new feature by running only the feature tests, which are influenced by their change is ultra important. Nowadays the tests of LibreOffice take more time than the compiliation.

Saving test and CPU time by triggering only the relevant features test would be great.
